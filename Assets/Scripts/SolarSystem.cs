﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;

public class SolarSystem : NetworkBehaviour {

	[SyncVar]
	string solarName;
	[SyncVar]
	float radius;
	int planetCount = 1;
	Vector3 starPos;

	[SyncVar]
	float size;

	[SyncVar]
	Color color;
	Canvas canvas;

	// Use this for initialization
	void Start () {
		canvas = GameObject.Find ("MainUICanvas").GetComponent<Canvas>();
		ApplyAppearanceStats ();
		PopulateSystem ();
	}

	public void SetAppearanceStats(){
		size = Random.Range (0.8f, 2f);
		float r = Random.Range (75, 100) / 100f;
		float g = Random.Range (10, 30) / 100f;
		float b = Random.Range (10, 30) / 100f;
		color = new Color (r, g, b, 1f);
		ApplyAppearanceStats ();
	}

	void ApplyAppearanceStats(){
		this.GetComponent<MeshRenderer> ().material.shader = Shader.Find ("Standard");
		this.GetComponent<MeshRenderer> ().material.color = color;
		transform.localScale = new Vector3 (size, size, size);
		this.gameObject.name = solarName;
		transform.parent = GameObject.FindGameObjectWithTag ("Galaxy").transform;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void SetName(string newName){
		solarName = newName;
	}

	public string GetName(){
		return solarName;
	}

	string GetPlanetRomanNumeral(int num){
		string numeral;
		switch (num) {
		case 1:
			numeral = "I";
			break;
		case 2:
			numeral = "II";
			break;
		case 3:
			numeral = "III";
			break;
		case 4:
			numeral = "IV";
			break;
		case 5:
			numeral = "V";
			break;
		default:
			numeral = "X";
			break;
		}
		return numeral;
	}

	public void SetPosition(Vector3 pos){
		starPos = pos;
		transform.position = starPos;
	}

	void PopulateSystem(){
		if (isServer) {
			for (int i = 1; i < 8; i++) {
				if (Random.Range (0, 2) == 1) {
					GameObject planet = CreatePlanet (solarName + " " + GetPlanetRomanNumeral (planetCount), i * size * 0.7f);
					NetworkServer.Spawn (planet);
				}
			}
		}
	}

	GameObject CreatePlanet(string planetName, float distance){
		//GameObject planet = GameObject.CreatePrimitive (PrimitiveType.Sphere);
		GameObject planet = (GameObject)Instantiate (Resources.Load ("Planet"));
		planet.name = planetName;
		planet.transform.parent = this.gameObject.transform;
		//planet.AddComponent<MeshCollider> ();
		//planet.AddComponent<Planet> ();

		float angle = Random.Range (0, 360);
		Vector3 spawnVec = PolarOffset (starPos, distance, angle);
		planet.GetComponent<Planet> ().SetPosition (spawnVec);
		planet.GetComponent<Planet> ().SetAngle (angle);
		planet.GetComponent<Planet> ().SetDistance (distance);
		planet.GetComponent<Planet> ().SetNames (planetName, solarName);
		planet.GetComponent<Planet> ().SetAppearanceStats ();

		if (isServer) {
			WorldManager.AddPlanetToList (planet);
		}
		planetCount++;
		return planet;
	}

	Vector3 PolarOffset(Vector3 center, float distance, float angle){
		float x = distance * Mathf.Cos (angle);
		float z = distance * Mathf.Sin (angle);
		Vector3 spawnVec = new Vector3 (center.x+x, 0, center.z+z);
		return spawnVec;
	}

	void OnMouseDown(){
		if (!canvas.GetComponent<CanvasUIScript>().hasOpenMenu){
			int pn = GetActingPlayerID ();
			GameObject player = GameObject.Find("World Cube").GetComponent<WorldManager>().GetPlayer (pn);
			player.GetComponent<ShipController>().MoveTo(player, this.gameObject);
			player.GetComponent<ShipController>().Target(this.gameObject);
			player.GetComponent<PlayGUIHandler>().SetTarget(this.gameObject);
			canvas.GetComponent<CanvasUIScript>().SetTarget(this.gameObject);
		}
	}

	void OnMouseOver(){
		if (Input.GetMouseButtonDown (1)) {
			int pn = GetActingPlayerID ();
			GameObject player = GameObject.Find("World Cube").GetComponent<WorldManager>().GetPlayer (pn);
			player.GetComponent<ShipController>().Target(this.gameObject);
			canvas.GetComponent<CanvasUIScript>().SetTarget(this.gameObject);
		}
	}

	int GetActingPlayerID(){
		for(int i = 0; i < 64; i++){
			if (GameObject.Find("World Cube").GetComponent<WorldManager>().GetPlayer (i).GetComponent<ShipController>().isLocalPlayer) {
				return i;
			}
		}
		return -999;
	}
}
