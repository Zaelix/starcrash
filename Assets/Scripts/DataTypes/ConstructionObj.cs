﻿using UnityEngine;
using System.Collections;

public class ConstructionObj {
	public int timeCost;
	public string name;
	public int count;

	public ConstructionObj(int tc, string n, int c){
		timeCost = tc;
		name = n;
		count = c;
	}

	public void SetCount(int c){
		count = c;
	}
}
