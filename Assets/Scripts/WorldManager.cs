﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections.Generic;
using System.Collections;

public class WorldManager : NetworkBehaviour {

	static int maxSystems = 250;
	static int systemCount = 0;
	static int planetCount = 0;
	public static int worldSize = 500;

	public int sc;
	public int pc;
	public int pas;

	static List<GameObject> players;
	public int playerCount;
	public GameObject background;

	string[] greekAlphabet;
	string[] constellationNames;
	static GameObject[] planets;

	// Use this for initialization
	void Start () {
		players = new List<GameObject> ();
		playerCount = 0;
		DefineArrays ();
		planets = new GameObject[maxSystems*5];
		 
		GenerateGalaxy ();
	}
	
	// Update is called once per frame
	void Update () {
		sc = systemCount;
		pc = planetCount;
		pas = planets.Length;
	}

	public void AddPlayer(GameObject player){
		if (player != null) {
			//Debug.Log ("Creating player" + playerCount + " object, in Player Array of length " + players.Count);
			players.Add (player);
			playerCount++;
			Debug.Log ("Player Object Added to Player List");
		} else {
			Debug.Log ("Null Player Object Found");
		}
	}

	public int GetPlayerCount(){
		return playerCount;
	}

	public GameObject GetPlayer(int playerNumber){
		if (players != null) {
			return players [playerNumber];
		} else {
			//GameObject fakePlayer = (GameObject)Instantiate (Resources.Load ("PlayerShip"));
			//fakePlayer.GetComponent<ShipController>().SetName("FakePlayer");
			return null;
		}
	}

	void DefineArrays(){
		greekAlphabet = new string[]{
			"Alpha", "Beta", "Gamma", "Delta", "Epsilon", "Zeta", "Eta", "Theta", "Iota", "Kappa", "Lambda", "Mu", 
			"Nu", "Xi", "Omicron", "Pi", "Rho", "Sigma", "Tau", "Upsilon", "Phi", "Chi", "Psi", "Omega"
		};
		constellationNames = new string[]{
			"Aegnor", "Amarie", "Amdir", "Amras", "Amrod", "Amroth", "Anaire", "Angrod", "Annael", "Aredhel", "Argon", 
			"Beleg", "Caranthir", "Celebon", "Celebrian", "Celebrimbor", "Celegorm", "Cirdan", "Curufin", "Daeron", "Denethor",
			"Duilin", "Earwin", "Earendil", "Ecthelion", "Edrahil", "Egalmoth", "Eldalote", "Elemmakil", "Elemmire", "Elenwe", 
			"Elu Thingol", "Elmo", "Elrond", "Enel", "Enelye", "Enerdhil", "Eol", "Erestor", "Feanor", "Finarfin", "Findis", 
			"Finduilas", "Fingolfin", "Fingon", "Finrod Felagund", "Finwe", "Galadhon", "Galadriel", "Galathil", "Galdor", 
			"Galion", "Gildor Inglorion", "Gil-galad", "Gimli", "Glorfindel", "Gwindor", "Haldir", "Idril", "Imin", "Iminye", 
			"Indis", "Inglor", "Ingwe", "Ingwion", "Irime", "Legolas", "Lindir", "Luthien Tinuviel", "Mablung", "Maedhros", 
			"Maeglin", "Maglor", "Mahtan", "Miriel", "Mithrellas", "Nellas", "Nerdanel", "Nimrodel", "Olwe", "Orodeth", 
			"Oropher", "Orophin", "Pengolodh", "Penlod", "Rog", "Rumil", "Saeros", "Salgant", "Tata", "Tatie", "Thrandiul", 
			"Turgon", "Tuor", "Voronwe"
		};
	}

	public static void AddPlanetToList(GameObject planet){
		//Debug.Log ("Current Planet Count: " + planetCount + ". Max Systems: " + maxSystems);
		planets [planetCount] = planet;
		planetCount++;
	}

	void GenerateGalaxy(){
		if (isServer) {
			Debug.Log ("Running Server mode: Generating Galaxy...");
			while (systemCount < maxSystems) {
				CreateSolarSystem ();
				systemCount++;
			}
			Debug.Log ("Galaxy Generated!");
		} else {
			Debug.Log ("Running Client mode...");
		}
	}

	void CreateSolarSystem(){
		string name = GenerateName ();
		int tryCount = 0;

		Vector3 spawnVec = PolarOffset (Random.Range (5, WorldManager.worldSize), Random.Range (0, 360));
		while(!SystemSpaceIsClear(spawnVec, 10) && tryCount < 100){
			spawnVec = PolarOffset (Random.Range (5, WorldManager.worldSize), Random.Range (0, 360));
			tryCount++;
		}
		if (tryCount >= 100) {
			Debug.Log ("Too many tries!");
		} else {
			//GameObject star = GameObject.CreatePrimitive (PrimitiveType.Sphere);
			GameObject star = (GameObject)Instantiate(Resources.Load("SolarSystem"));
			star.name = name;
			//star.AddComponent<MeshCollider> ();
			//star.AddComponent<SolarSystem> ();
			star.GetComponent<SolarSystem> ().SetPosition (spawnVec);
			star.GetComponent<SolarSystem> ().SetName (name);
			star.GetComponent<SolarSystem> ().SetAppearanceStats ();
			NetworkServer.Spawn (star);
		}
	}

	bool SystemSpaceIsClear(Vector3 pos, float range){
		bool result = false;
		Collider[] check = Physics.OverlapSphere (pos, range);
		int count = 0;

		for (int i = 0; i < check.Length; i++){
			if (check [i].GetComponent<Planet> () != null || check [i].GetComponent<SolarSystem> () != null) {
				count++;
			}
		}
		if (count == 0) {
			result = true;
		}
		return result;
	}

	string GenerateName(){
		string name1 = greekAlphabet [Random.Range (0, greekAlphabet.Length)];
		string name2 = constellationNames [Random.Range (0, constellationNames.Length)];
		string newName = name1 + " " + name2;
		return newName;
	}

	Vector3 PolarOffset(float distance, float angle){
		float x = distance * Mathf.Cos (angle);
		float z = distance * Mathf.Sin (angle);
		Vector3 spawnVec = new Vector3 (x, 0, z);
		return spawnVec;
	}
}
