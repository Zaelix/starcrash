﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using System.Text;

public class Planet : NetworkBehaviour {

	public GameObject mesh;
	public Texture tex;
	Rigidbody rb;

	GameObject friendFoeMarker;
	[SyncVar]
	string ownerName = "AI";
	[SyncVar]
	string solarName;
	[SyncVar]
	string parentStarName;
	[SyncVar]
	float size;
	float richness;

	// Ship Counts
	[SyncVar]
	public int fighters = 0;
	private int frigates = 0;
	private int cruisers = 0;
	private int destroyers = 0;
	private int ancients = 0;

	// Building Counts
	private int factories = 0;
	private int beacons = 0;
	private int doomsdays = 0;
	private int arrays = 0;

	Queue<ConstructionObj> queue = new Queue<ConstructionObj> ();
	public int queueWorkCounter = 0;
	int queueItemCount = 1;

	[SyncVar]
	public float orbitSpeed;

	[SyncVar]
	float angle;
	[SyncVar]
	float distance;
	[SyncVar]
	Color color;

	Canvas canvas;
	Text fleetFighters;
	Slider fighterSldr;
	Text planetFighters;

	// Use this for initialization
	void Start () {
		canvas = GameObject.Find ("MainUICanvas").GetComponent<Canvas>();
		ApplyAppearanceStats ();
		SetUILinks ();
	}

	void SetUILinks(){
		fleetFighters = canvas.transform.GetChild (2).transform.GetChild (9).transform.GetChild (5).GetComponent<Text> ();
		fighterSldr = canvas.transform.GetChild (2).transform.GetChild (9).transform.GetChild (14).GetComponent<Slider> ();
		planetFighters = canvas.transform.GetChild (2).transform.GetChild (9).transform.GetChild (0).GetComponent<Text> ();
	}

	public void SetAppearanceStats(){
		size = Random.Range (0.3f, 0.5f);
		float r = Random.Range (0, 20) / 100f;
		float g = Random.Range (25, 100) / 100f;
		float b = 1 - g;
		color = new Color (r, g, b, 1f);
		orbitSpeed = Random.Range (3, 10)/2000f;
		ApplyAppearanceStats ();
	}

	void ApplyAppearanceStats(){
		this.gameObject.name = solarName;
		this.transform.parent = GameObject.Find (parentStarName).transform;
		transform.localScale = new Vector3(size, size, size);
		this.GetComponent<MeshRenderer> ().material.shader = Shader.Find ("Standard");
		this.GetComponent<MeshRenderer> ().material.color = color;

		if (friendFoeMarker != null) {
			return;
		} else {
			friendFoeMarker = CreateFriendFoeMarker ();
		}
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		Orbit ();
		if (queue.Count > 0) {
			ConstructQueue ();
		}
	}

	GameObject CreateFriendFoeMarker(){
		GameObject mark = (GameObject)Instantiate (Resources.Load ("FriendFoePlanetMarker"));
		mark.name = "FriendFoeMarker";
		mark.transform.position = transform.position + new Vector3 (-0.4f*size-0.4f, 0.4f, 0);
		float mScale = 0.5f / size;
		//mark.transform.localScale = new Vector3 (mScale,mScale,1);
		mark.transform.localScale = new Vector3 (2f,2f,1);
		mark.transform.parent = this.gameObject.transform;
		mark.GetComponent<SpriteRenderer> ().color = new Color (1, 1, 1, 0); //Color.white, fully transparent;
		return mark;
	}

	public void SetFriendlyMarker(bool friendly){
		if (friendly) {
			friendFoeMarker.GetComponent<SpriteRenderer> ().color = Color.green;
		} else if (!friendly) {
			friendFoeMarker.GetComponent<SpriteRenderer> ().color = Color.red;
		}
	}

	public void SetNoMarker(){
		friendFoeMarker.GetComponent<SpriteRenderer> ().color = new Color (1, 1, 1, 0);
	}

	void ConstructQueue(){
		if (queueWorkCounter >= queue.Peek ().timeCost) {
			if (queue.Peek ().count > 1) {
				queueItemCount++;
				queue.Peek ().SetCount (queue.Peek ().count-1);
				Construct(queue.Peek());
			} else {
				ConstructionObj dq = queue.Dequeue ();
				Construct(dq);
				dq.SetCount (queueItemCount);
				AddToQueue (dq);
				queueItemCount = 1;
			}
			queueWorkCounter = 0;
		} else {
			queueWorkCounter++;
		}
	}

	void Construct(ConstructionObj obj){
		switch (obj.name) {
		case "Fighter":
			fighters++;
			break;
		case "Frigate":
			frigates++;
			break;
		case "Cruiser":
			cruisers++;
			break;
		case "Destroyer":
			destroyers++;
			break;
		case "Ancient":
			ancients++;
			break;
		case "Factory":
			factories++;
			break;
		case "Beacon":
			beacons++;
			break;
		case "Doomsday":
			doomsdays++;
			break;
		case "Array":
			arrays++;
			break;
		default:
			break;
		}
	}

	public void AddToQueue(ConstructionObj obj){
		queue.Enqueue (obj);
	}

	public ConstructionObj GetTopOfQueue(){
		return queue.Peek ();
	}

	public ConstructionObj[] GetQueueArray(){
		ConstructionObj[] arr = new ConstructionObj[queue.Count];
		queue.CopyTo (arr, 0);
		return arr;
	}

	void CheckForQueueStack(string name, int timeCost){
		if (this.GetQueueArray ().Length > 0 && this.GetTopOfQueue ().name == name) {
			int c = this.GetTopOfQueue ().count;
			this.GetTopOfQueue ().SetCount (c + 1);
		} else {
			this.AddToQueue (new ConstructionObj (timeCost, name, 1));
		}
	}

	public void AddItemToQueue(int i){
		switch (i) {
		case 0:
			CheckForQueueStack ("Fighter", 100);
			break;
		case 1:
			CheckForQueueStack ("Frigate", 200);
			break;
		case 2:
			CheckForQueueStack ("Cruiser", 300);
			break;
		case 3:
			CheckForQueueStack ("Destroyer", 400);
			break;
		case 4:
			CheckForQueueStack ("Factory", 2000);
			break;
		case 5:
			CheckForQueueStack ("Beacon", 2600);
			break;
		case 6:
			CheckForQueueStack ("Doomsday", 1500);
			break;
		case 7:
			CheckForQueueStack ("Array", 20000);
			break;
		default:
			break;
		}
	}

	void Orbit(){
		angle += orbitSpeed;
		SetPosition (PolarOffset(distance, angle));
	}

	public string GetOwnerName(){
		return ownerName;
	}

	[Command]
	public void CmdSetNewOwner(string newOwner){
		//Debug.Log (newOwner);
		ownerName = newOwner;
	}

	public void SetPosition(Vector3 newPos){
		this.transform.position = newPos;
	}

	public void SetNames(string newName, string starName){
		solarName = newName;
		parentStarName = starName;
	}

	public string GetName(){
		return solarName;
	}

	public void SetAngle (float ang){
		angle = ang;
	}

	public float GetAngle(){
		return angle;
	}
		
	public void SetDistance (float dist){
		distance = dist;
	}

	public float GetDistance (){
		return distance;
	}

	public int[] GetShips(){
		int[] ships = new int[5];
		ships [0] = fighters;
		ships [1] = frigates;
		ships [2] = cruisers;
		ships [3] = destroyers;
		ships [4] = ancients;
		return ships;
	}

	public void SetShips(int[] ships){
		fighters = ships [0];
		frigates = ships [1];
		cruisers = ships [2];
		destroyers = ships [3];
		ancients = ships [4];
	}
		
	public void SetFighters(GameObject player){
		int temp = this.fighters + player.GetComponent<ShipController>().GetShips()[0];
		fleetFighters.text = "Fighters: " + (temp - (fighterSldr.value * temp));
		planetFighters.text = "Fighters: " + fighterSldr.value * temp;
	}

	public int[] GetBuildings(){
		int[] buildings = new int[4];
		buildings [0] = factories;
		buildings [1] = beacons;
		buildings [2] = doomsdays;
		buildings [3] = arrays;
		return buildings;
	}

	public void SetBuildings(int[] buildings){
		factories = buildings [0];
		beacons = buildings [1];
		doomsdays = buildings [2];
		arrays = buildings [3];
	}

	Vector3 PolarOffset(float distance, float angle){
		Vector3 center = this.transform.parent.position;
		float x = distance * Mathf.Cos (angle);
		float z = distance * Mathf.Sin (angle);
		Vector3 spawnVec = new Vector3 (center.x+x, 0, center.z+z);
		return spawnVec;
	}

	void OnMouseDown(){
		if (!canvas.GetComponent<CanvasUIScript>().hasOpenMenu){
			int pn = GetActingPlayerID ();
			GameObject player = GameObject.Find("World Cube").GetComponent<WorldManager>().GetPlayer (pn);
			player.GetComponent<ShipController>().MoveTo(player, this.gameObject);
			player.GetComponent<ShipController>().Target(this.gameObject);
			player.GetComponent<PlayGUIHandler>().SetTarget(this.gameObject);
			canvas.GetComponent<CanvasUIScript>().SetTarget(this.gameObject);
		}
	}

	void OnMouseOver(){
		if (Input.GetMouseButtonDown (1)) {
			int pn = GetActingPlayerID ();
			GameObject player = GameObject.Find("World Cube").GetComponent<WorldManager>().GetPlayer (pn);
			player.GetComponent<ShipController>().Target(this.gameObject);
			canvas.GetComponent<CanvasUIScript>().SetTarget(this.gameObject);
		}
	}

	int GetActingPlayerID(){
		for(int i = 0; i < 64; i++){
			if (GameObject.Find("World Cube").GetComponent<WorldManager>().GetPlayer (i).GetComponent<ShipController>().isLocalPlayer) {
				return i;
			}
		}
		return -999;
	}
}
