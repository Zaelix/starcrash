﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;

public class ShipController : NetworkBehaviour {

	Vector3 targetDelta = new Vector3(0,0,0);
	GameObject tar;
	float speed = 0.1f;
	GameObject mainCam;
	GameObject worldCube;
	GameObject targetIndicator;
	//Quaternion rot;

	Collider[] objectsInScannerRange;

	[SyncVar]
	public string playerName = "Player";
	[SyncVar]
	public int playerNumber = -999;

	private int fighters = 0;
	private int frigates = 0;
	private int cruisers = 0;
	private int destroyers = 0;
	private int ancients = 0;

	[SyncVar]
	public bool isDockedAtPlanet = false;
	private GameObject dockedAt;

	// Use this for initialization
	public override void OnStartLocalPlayer () {
		GameObject.Find ("MainUICanvas").GetComponent<CanvasUIScript>().SetPlayerObject(this);
	}

	public void Start () {
		mainCam = GameObject.FindGameObjectWithTag ("MainCamera");
		tar = this.gameObject;
		worldCube = GameObject.FindGameObjectWithTag ("World Cube");
		targetIndicator = (GameObject)Instantiate (Resources.Load ("TargetIndicator"));
		targetIndicator.GetComponent<SpriteRenderer> ().color = new Color (1, 1, 1, 0);
		targetIndicator.transform.localScale = new Vector3 (0.5f, 0.5f, 0.5f);
		playerNumber = worldCube.GetComponent<WorldManager> ().GetPlayerCount ();
		worldCube.GetComponent<WorldManager> ().AddPlayer (this.gameObject);
		Debug.Log ("Player Object Created");

	}
	
	// Update is called once per frame
	void Update () {
		if (!isLocalPlayer) {
			return;
		}
		Scan ();
		MoveTowards ();
	}

	public void SetName(string nm){
		playerName = nm;
		this.gameObject.name = "Player_" + playerName;
	}

	void Scan(){
		CheckPlanetOwners ();
		if (Input.GetKeyDown (KeyCode.Space)) {
			Debug.Log (playerName + " is scanning SPACE!!");
			CheckPlanetOwners ();
		}
	}

	public void MoveTo(GameObject playerObj, GameObject target){
		tar = target;
		targetDelta = (playerObj.transform.position - new Vector3(0,1,0)) - tar.transform.position;
		FaceTowards (playerObj, tar);
	}

	void MoveTowards(){
		mainCam.transform.position = this.gameObject.transform.position + new Vector3 (0, 10, 0);
		if (targetDelta.magnitude > 0.1) {
			FaceTowards(this.gameObject, tar);
			isDockedAtPlanet = false;
			transform.position = transform.position - targetDelta.normalized*speed;
			targetDelta = (this.gameObject.transform.position - new Vector3 (0, 1, 0)) - tar.transform.position;
			dockedAt = null;
		} else {
			if (tar.GetComponent<Planet>() != null) {
				isDockedAtPlanet = true;
				CmdTakePlanet (this.playerName);
			}
			dockedAt = tar;
			transform.position = new Vector3(tar.transform.position.x, 1, tar.transform.position.z);
		}
	}

	public void FaceTowards(GameObject playerObj, GameObject target){
		playerObj.transform.LookAt (target.transform.position + new Vector3(0,1,0));
		playerObj.transform.Rotate (new Vector3 (0, 90, 0));
	}

	public void Target(GameObject obj){
		targetIndicator.transform.position = obj.transform.position + new Vector3 (0, 0.5f, 0);
		targetIndicator.GetComponent<SpriteRenderer> ().color = new Color (1, 1, 1, 1);
		targetIndicator.transform.parent = obj.transform;
	}

	[Command]
	void CmdTakePlanet(string pName){
		int[] ships = tar.GetComponent<Planet> ().GetShips ();
		if (ships [0] == 0 && ships [1] == 0 && ships [2] == 0 && ships [3] == 0 && ships [4] == 0) {
			//string pName = this.gameObject.GetComponent<ShipController> ().playerName;
			tar.GetComponent<Planet> ().CmdSetNewOwner (pName);
		}
	}

	void CheckPlanetOwners(){
		objectsInScannerRange = Physics.OverlapSphere (this.transform.position, 30);
		for (int i = 0; i < objectsInScannerRange.Length; i++) {
			if(objectsInScannerRange[i].GetComponent<Planet>() != null){
				string ownerName = objectsInScannerRange [i].GetComponent<Planet> ().GetOwnerName ();
				if (ownerName != this.playerName && ownerName != "AI") {
					objectsInScannerRange [i].GetComponent<Planet> ().SetFriendlyMarker (false);
				} else if (ownerName == playerName) {
					objectsInScannerRange [i].GetComponent<Planet> ().SetFriendlyMarker (true);
				} else if (ownerName == "AI") {
					objectsInScannerRange [i].GetComponent<Planet> ().SetNoMarker();
				}
			}
		}
	}

	public bool IsDockedAtPlanet(){
		return isDockedAtPlanet;
	}

	public int[] GetShips(){
		int[] ships = new int[5];
		ships [0] = fighters;
		ships [1] = frigates;
		ships [2] = cruisers;
		ships [3] = destroyers;
		ships [4] = ancients;
		return ships;
	}

	public void SetShips(int[] ships){
		fighters = ships [0];
		frigates = ships [1];
		cruisers = ships [2];
		destroyers = ships [3];
		ancients = ships [4];
	}

	public void SetShipsOnPlanet(){
		int pn = GetActingPlayerID ();
		ShipController player = GameObject.Find("World Cube").GetComponent<WorldManager>().GetPlayer (pn).GetComponent<ShipController>();
		if (!isLocalPlayer) {
			return;
		}
		CmdSetShipsOnPlanet ();
	}

	[Command]
	public void CmdSetShipsOnPlanet(){
		dockedAt.GetComponent<Planet> ().SetFighters (this.gameObject);
	}

	int GetActingPlayerID(){
		for(int i = 0; i < 64; i++){
			if (GameObject.Find ("World Cube").GetComponent<WorldManager> ().GetPlayer (i) != null) {
				ShipController plyr = GameObject.Find ("World Cube").GetComponent<WorldManager> ().GetPlayer (i).GetComponent<ShipController> ();
				if (plyr != null && plyr.isLocalPlayer) {
					return i;
				}
			}
		}
		return -99;
	}
}
