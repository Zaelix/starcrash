﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;

public class PlayGUIHandler : NetworkBehaviour {
	// Common variables
	float xCenter = Screen.width/2;
	float yCenter = Screen.height/2;

	//Target Display Rectangles
	Rect tarDisplayText = new Rect(5, 5, 60, 20);
	Rect tarDisplayField = new Rect(65, 5, 160, 20);
	string targetName;

	GameObject targetObj;
	Planet target;
	ShipController player;

	// Planet Menu positions etc
	Rect enterPlanetButtonRect;
	Rect planetBuildMenu;
	Rect planetBuildButtons;
	int selectedConstructionButtonIndex = -1;
	int selectedTransferButtonIndex = -1;
	string[] planetBuildButtonNames;

	// Menu Info Rectangles
	Rect planetShipInfoBox;
	Rect fleetShipInfoBox;
	Rect planetQueueBox;

	// Ship Transfer Menu Button Rectangles
	Rect shipTransferMenuLeft;

	// Menu Textures
	public Texture iTex;
	public Texture[] constructionButtonTextures = new Texture[8];
	public Texture[] shipTransferButtonsLeftTextures = new Texture[5];

	// Menu Content Objects
	//GUIContent[] planetShipContents = new GUIContent[4];

	GUIStyle style = new GUIStyle();

	bool showDockedMenu = false;

	// Use this for initialization
	void Start () {
		GUI.backgroundColor = Color.clear;
		style.fontSize = 20;
		style.stretchWidth = true;
		style.stretchHeight = true;
		RectOffset offset = new RectOffset ();
		offset.bottom = 15;
		style.margin = offset;
		//player = WorldManager.players [0].GetComponent<ShipController> ();
		player = this.GetComponent<ShipController>();

		enterPlanetButtonRect = new Rect (Screen.width - 110, 5, 100, 20);
		planetBuildMenu = new Rect(20, Screen.height*0.375f, Screen.width*0.5f, Screen.height*0.5f);
		planetBuildButtons = new Rect(5, 18, Screen.width*0.5f, Screen.height*0.45f);
		//new Rect(5,18,planetBuildMenu.width, planetBuildMenu.height)
		planetShipInfoBox = new Rect(Screen.width*0.55f, (yCenter - yCenter/4), Screen.width*0.10f, yCenter);
		fleetShipInfoBox = new Rect(Screen.width*0.90f, (yCenter - yCenter/4), Screen.width*0.1f, yCenter);
		planetQueueBox = new Rect(Screen.width - xCenter/2 + 30, 30, xCenter/2 - 40, yCenter/2);

		shipTransferMenuLeft = new Rect(Screen.width*0.65f, Screen.height*0.41f, Screen.width*0.05f, Screen.height*0.22f);

		planetBuildButtonNames = new string[8];
		planetBuildButtonNames [0] = "Fighter";
		planetBuildButtonNames [1] = "Frigate";
		planetBuildButtonNames [2] = "Cruiser";
		planetBuildButtonNames [3] = "Destroyer";
		planetBuildButtonNames [4] = "Factory";
		planetBuildButtonNames [5] = "Beacon";
		planetBuildButtonNames [6] = "Doomsday Device";
		planetBuildButtonNames [7] = "Warp Array";
	}
	
	// Update is called once per frame
	void Update () {
		CheckConstructionButtons ();
	}

	void CheckConstructionButtons(){
		if (targetObj != null) {
			//int[] shipCounts;
			switch (selectedConstructionButtonIndex) {
			case 0:
				CheckForQueueStack ("Fighter", 100);
				selectedConstructionButtonIndex = -1;
				break;
			case 1:
				CheckForQueueStack ("Frigate", 200);
				selectedConstructionButtonIndex = -1;
				break;
			case 2:
				CheckForQueueStack ("Cruiser", 300);
				selectedConstructionButtonIndex = -1;
				break;
			case 3:
				CheckForQueueStack ("Destroyer", 400);
				selectedConstructionButtonIndex = -1;
				break;
			case 4:
				CheckForQueueStack ("Factory", 2000);
				selectedConstructionButtonIndex = -1;
				break;
			case 5:
				CheckForQueueStack ("Beacon", 2600);
				selectedConstructionButtonIndex = -1;
				break;
			case 6:
				CheckForQueueStack ("Doomsday", 1500);
				selectedConstructionButtonIndex = -1;
				break;
			case 7:
				CheckForQueueStack ("Array", 20000);
				selectedConstructionButtonIndex = -1;
				break;
			default:
				break;
			}
		}
	}

	void CheckForQueueStack(string name, int timeCost){
		if (target.GetQueueArray ().Length > 0 && target.GetTopOfQueue ().name == name) {
			int c = target.GetTopOfQueue ().count;
			target.GetTopOfQueue ().SetCount (c + 1);
		} else {
			target.AddToQueue (new ConstructionObj (timeCost, name, 1));
		}
	}

	void OnGUI(){
		GUI.Label (tarDisplayText, "Target: ");
		GUI.Label (tarDisplayField, targetName);

		if (player.IsDockedAtPlanet ()) {
			if (GUI.Button (enterPlanetButtonRect, "Dock/Undock")) {
				if (!showDockedMenu) {
					CmdGetShipCountsFromServer ();
					showDockedMenu = true;
				} else
					showDockedMenu = false;
			}
		} else {
			showDockedMenu = false;
		}
		if (showDockedMenu) {
			GUI.Window (0, planetBuildMenu, ShowPlanetMenu, "Construction Options");
			GUI.DrawTexture (planetShipInfoBox, iTex);
			GUI.Window (1, planetShipInfoBox, ShowPlanetInfo, "Ships On Planet");
			GUI.DrawTexture (fleetShipInfoBox, iTex);
			GUI.Window (2, fleetShipInfoBox, ShowFleetInfo, "Ships In Fleet");
			GUI.DrawTexture (planetQueueBox, iTex);
			GUI.Window (3, planetQueueBox, ShowPlanetQueue, "Planet Queue");

			// Ship Transfer buttons
			selectedTransferButtonIndex = GUI.SelectionGrid(shipTransferMenuLeft, selectedTransferButtonIndex, shipTransferButtonsLeftTextures, 1, style);

		}
	}

	[Command]
	void CmdGetShipCountsFromServer(){
		
	}

	public void showDockUI(){
		Debug.Log ("Canvas Button Clicked");
	}

	public void SetTarget(GameObject tar){
		targetObj = tar;
		if(tar.GetComponent<SolarSystem> () != null){
			targetName = tar.GetComponent<SolarSystem> ().GetName ();
			target = targetObj.GetComponent<Planet> ();
		}
		else if(tar.GetComponent<Planet> () != null){
			targetName = tar.GetComponent<Planet> ().GetName ();
			target = targetObj.GetComponent<Planet> ();
		}
	}

	void ShowPlanetMenu(int windowID){
		selectedConstructionButtonIndex = GUI.SelectionGrid(planetBuildButtons, selectedConstructionButtonIndex, constructionButtonTextures, 4, style);
	}

	void ShowPlanetInfo(int windowID){
		int[] planetShips = targetObj.GetComponent<Planet> ().GetShips ();
		GUI.Label (new Rect(10, 20, 100, 20), "Fighters: " + planetShips[0]);
		GUI.Label (new Rect(10, 50, 100, 20), "Frigates: " + planetShips[1]);
		GUI.Label (new Rect(10, 80, 100, 20), "Cruisers: " + planetShips[2]);
		GUI.Label (new Rect(10, 110, 100, 20), "Destroyers: " + planetShips[3]);
		GUI.Label (new Rect(10, 140, 100, 20), "Ancients: " + planetShips[4]);
	}

	// Currently unused
	GUIContent[] GetPlanetInfoContents(){
		int[] planetShips = targetObj.GetComponent<Planet> ().GetShips ();
		GUIContent[] contents = new GUIContent[5];
		contents [0] = new GUIContent("Fighters: " + planetShips[0], iTex);
		contents [1] = new GUIContent("Frigates: " + planetShips[1], iTex);
		contents [2] = new GUIContent("Cruisers: " + planetShips[2], iTex);
		contents [3] = new GUIContent("Destroyers: " + planetShips[3], iTex);
		contents [4] = new GUIContent("Ancients: " + planetShips[4], iTex);
		return contents;
	}

	void ShowFleetInfo(int windowID){
		int[] fleetShips = player.GetShips ();
		GUI.Label (new Rect(10, 20, 100, 20), "Fighters: " + fleetShips[0]);
		GUI.Label (new Rect(10, 50, 100, 20), "Frigates: " + fleetShips[1]);
		GUI.Label (new Rect(10, 80, 100, 20), "Cruisers: " + fleetShips[2]);
		GUI.Label (new Rect(10, 110, 100, 20), "Destroyers: " + fleetShips[3]);
		GUI.Label (new Rect(10, 140, 100, 20), "Ancients: " + fleetShips[4]);
	}

	void ShowPlanetQueue(int windowID){
		ConstructionObj[] queue = target.GetQueueArray();
		if (queue.Length > 0){
			GUI.Label (new Rect (xCenter/2 - 130, 20, 100, 20), target.queueWorkCounter + "/" + queue [0].timeCost);
		}
		for (int i = 0; i < queue.Length; i++){
			GUI.Label(new Rect(10, (i*30)+20, 100, 20), queue[i].name + " x" + queue[i].count);
		}
	}
}
