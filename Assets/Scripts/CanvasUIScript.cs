﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CanvasUIScript : MonoBehaviour {

	public Canvas canvas;
	public ShipController player;
	public bool hasOpenMenu = false;

	string playerName = "Default";

	Button dockButton;
	GameObject target;
	string targetName;
	Text targetDisplay;
	Text ownerDisplay;

	CanvasRenderer targetInfoPanel;
	CanvasRenderer nameInputPanel;
	CanvasRenderer dockingMenuPanel;

	// Use this for initialization
	void Start () {
		canvas = GameObject.Find ("MainUICanvas").GetComponent<Canvas>();
		targetDisplay = canvas.transform.GetChild(0).transform.GetChild(0).GetComponent<Text>();
		ownerDisplay = canvas.transform.GetChild(0).transform.GetChild(1).GetComponent<Text>();
		dockButton = canvas.transform.GetChild(1).GetComponent<Button>();
		dockingMenuPanel = canvas.transform.GetChild(2).GetComponent<CanvasRenderer>();
		targetInfoPanel = canvas.transform.GetChild (0).GetComponent<CanvasRenderer>();
		nameInputPanel = canvas.transform.GetChild (3).GetComponent<CanvasRenderer>();


		dockingMenuPanel.gameObject.SetActive (false);
	}
	
	// Update is called once per frame
	void Update () {

		if (player != null && player.IsDockedAtPlanet ()) {
			dockButton.gameObject.SetActive (true);
		} else {
			dockButton.gameObject.SetActive (false);
		}

		if (dockingMenuPanel.gameObject.activeSelf) {
			hasOpenMenu = true;

		} else {
			hasOpenMenu = false;
		}
	}
		

	public void SetTarget(GameObject tar){
		target = tar;
		targetDisplay.text = "Target: " + target.name;
		if (target.GetComponent<Planet> () != null) {
			ownerDisplay.text = "Owner: " + target.GetComponent<Planet> ().GetOwnerName ();
		} else {
			ownerDisplay.text = "Owner: None";
		}
	}

	public void SetPlayerObject(ShipController plr){
		player = plr;
		player.SetName (playerName);
		targetInfoPanel.gameObject.SetActive (true);
		nameInputPanel.gameObject.SetActive (false);
	}

	public void SetPlayerName(){
		playerName = canvas.transform.GetChild(3).transform.GetChild(0).transform.GetChild(1).GetComponent<Text>().text;
	}

	public void OpenDockMenu(){
		dockingMenuPanel.gameObject.SetActive (!dockingMenuPanel.gameObject.activeSelf);
	}

}
